This is a simple API

- API version: 1.0.0
- Package version: 1.0.0

## Requirements.

Python 2.7 and 3.4+

## Installation & Usage
### pip install

If the python package is hosted on Github, you can install directly from Github

```sh
pip install git+https://github.com//.git
```
(you may need to run `pip` with root permission: `sudo pip install git+https://github.com//.git`)


### Setuptools

Install via [Setuptools](http://pypi.python.org/pypi/setuptools).

```sh
python setup.py install --user
```
(or `sudo python setup.py install` to install the package for all users)



## Getting Started

Please follow the [installation procedure](#installation--usage) and then run the following:



## Documentation for API Endpoints

All URIs are relative to *http://127.0.0.1:8000/*

Class  | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AuthorDetailView*  | **PUT** /author/{id} | replace author
*AuthorDetailView* | **PATCH** /author/{id} | update author
*AuthorDetailView*  | **GET** /author/{id} | searches author
*AuthorView*  | **GET** /author/ | search authors
*AuthorView*  | **POST** /author/ | add new author
*BookView* | **GET** /book/ | searches book
*BookView*  | **POST** /book/ | add new book
*BookDetailView*  | **GET** /book/{id}/ | searches book
*BookDetailView*  | **PATCH** /book/{id}/ | update book
*BookDetailView*  | **PUT** /book/{id}/ | replace book
*LibraryView*  | **POST** /library/ | add new library
*LibraryView*  | **PATCH** /library/ | update library


## Documentation For Models

 - [Author](libraryapp/library/models.py)
 - [Book](libraryapp/library/models.py)


## Documentation For Authorization

 All endpoints do not require authorization.


## Author

hakan.degirmen@metglobal.com

