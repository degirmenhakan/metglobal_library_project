import json
from datetime import datetime
from django.core.urlresolvers import reverse

from rest_framework.views import APIView
from rest_framework.response import Response

from rest_framework import viewsets, status
from rest_framework.decorators import list_route
from rest_framework.settings import api_settings
from rest_framework_csv.parsers import CSVParser
from rest_framework_csv.renderers import CSVRenderer

from libraryapp.library.serializers import BookSerializer, AuthorSerializer
from libraryapp.library.models import Book, Author
from libraryapp.library.helpers import book_create, add_authors, create_author

class BookView(APIView):
    def get(self, request):
        try:
            books = Book.objects.filter(lc_classification__iregex=\
                                        request.GET.get('filter', r'.*'))
            lib_obj = []
            for book in books.all():
                authors = []
                for author in book.authors.all():
                    authors.append({'name':author.name, 'surname':author.surname,
                                    'birth_day':author.birth_day})
                obj = {
                    'id':book.id,
                    'title':book.title,
                    'lc_classification':book.lc_classification,
                    'authors':authors
                }

                lib_obj.append(obj)
            return Response(lib_obj)
        except:
            return Response({'message': e.message,'status':401})
    def post(self, request, *args, **kwargs):
        try:
            data = request.POST.get('data')
            new_book = book_create(data)
            new_book.save()
            add_authors(new_book, data)
            new_book.save()
            return Response({'message': 'Successfull'})
        except Exception as e:
            return Response({'message': e.message,'status':401})


class BookDetailView(APIView):

    def get(self, request, pk):

        try:
            book = Book.objects.get(pk=pk)
            book_obj = []
            authors = []
            for author in book.authors.all():
                authors.append({'name':author.name, 'surname':author.surname,
                                'birth_day':author.birth_day})
            obj = {
                'title':book.title,
                'lc_classification':book.lc_classification,
                'authors':authors
            }

            book_obj.append(obj)
            return Response(book_obj)
        except Exception as e:
            return Response({'message': e.message,'status':401})

    def patch(self, request, pk):
        try:
            book = Book.objects.get(pk=pk)
            data = request.POST.get('data')
            #parsing
            infos = data.split(',')
            book.title = infos[0]
            book.lc_classification = infos[1]
            book.save()
            book.authors = []
            infos = data.split(',')[2:]
            author_num = 0
            if len(infos) % 3 == 0:
                while (author_num*3) < len(infos):
                    author = create_author(infos[author_num*3:])
                    book.authors.add(author)
                    book.save()
                    author_num += 1
            book.save()
            return Response({'message': 'Successfull'})
        except Exception as e:
            return Response({'message': e.message,'status':401})


    def put(self, request, pk):
        try:
            book = Book.objects.get(pk=pk)
            data = request.POST.get('data')
            #parsing
            infos = data.split(',')
            book.title = infos[0]
            book.lc_classification = infos[1]
            book.save()
            book.authors = []
            infos = data.split(',')[2:]
            author_num = 0
            if len(infos) % 3 == 0:
                while (author_num*3) < len(infos):
                    author = create_author(infos[author_num*3:])
                    book.authors.add(author)
                    book.save()
                    author_num += 1
            book.save()
            return Response({'message': 'Successfull'})
        except Exception as e:
            return Response({'message': e.message,'status':401})


class AuthorView(APIView):

    def get(self, request):
        try:
            author_obj = []
            authors = Author.objects.filter(name__iregex=\
                                        request.GET.get('filter', r'.*'))
            for author in authors:
                author_obj.append({'name':author.name, 'surname':author.surname,
                'birth_day':author.birth_day, 'id':author.id})
            return Response(author_obj)
        except Exception as e:
             return Response({'message':e.message,'status':400})
    def post(self, request):
        try:
            data = request.POST.get('data')
            infos = data.split(',')
            name = infos[0]
            surname = infos[1]
            birth_day = datetime.strptime(infos[2], '%Y-%m-%d')
            author = Author.objects.get_or_create(name=name, surname=surname, \
                                                    birth_day=birth_day)

            if author[1] == False:
                return Response({'message':'This author already exists', \
                                'status':401})
            else:
                return Response({'message':'Success', 'status':200})
        except Exception as e:
            return Response({'message': e.message,'status':401})


class AuthorDetailView(APIView):

    def get(self, request, pk):
        try:
            author = Author.objects.get(pk=pk)
            obj = {
                'name':author.name,
                'surname':author.surname,
                'birth_day':author.birth_day
            }

            return Response(obj)
        except Exception as e:
             return Response({'message': e.message,'status':401})

    def patch(self, request, pk):
        try:
            data = request.POST.get('data')
            author = Author.objects.get(pk=pk)
            infos = data.split(',')
            author.name = infos[0]
            author.surname = infos[1]
            author.birth_day = datetime.strptime(infos[2], '%Y-%m-%d')
            author.save()
            return Response({'message':'successfully','status':200})
        except Exception as e:
            return Response({'message':e.message,'status':401})


    def put(self, request, pk):
        try:
            data = request.POST.get('data')
            author = Author.objects.get(pk=pk)
            infos = data.split(',')
            author.name = infos[0]
            author.surname = infos[1]
            author.birth_day = datetime.strptime(infos[2], '%Y-%m-%d')
            author.save()
            return Response({'message':'successfully','status':200})
        except Exception as e:
            return Response({'message':e.message,'status':401})

class LibraryView(APIView):
    def post(self, request):
        if len(request.FILES) == 0:
            return Response({'error': 'no csv file '})
        try:
            val = ''
            for csv_file in request.FILES.values():
                val += csv_file.read().strip()
            line_list = val.strip().split('\n')
            Author.objects.all().delete()
            Book.objects.all().delete()
            for line in line_list:
                new_book = book_create(line)
                new_book.save()
                add_authors(new_book, line)

        except Exception:
            return Response({'error': 'Unsuccessfull'})
        return Response({'message': 'Successfull'})

    def patch(self, request):
        if len(request.FILES) == 0:
            return Response({'error': 'no csv file '})
        try:
            val = ''
            for csv_file in request.FILES.values():
                val += csv_file.read().strip()
            line_list = val.strip().split('\n')
            Author.objects.all().delete()
            Book.objects.all().delete()
            for line in line_list:
                new_book = book_create(line)
                new_book.save()
                add_authors(new_book, line)

        except Exception:
            return Response({'error': 'Unsuccessfull'})
        return Response({'message': 'Successfull'})
