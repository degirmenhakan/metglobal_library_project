from __future__ import unicode_literals

from django.db import models

# Create your models here.
'''
Author:
    Name
    Surname
    Date of birth
'''
class Author(models.Model):
    name = models.CharField(max_length=30)
    surname = models.CharField(max_length=30)
    birth_day = models.DateField(blank=True)

    def __uniceode__(self):
        return name,' ',surname,' ',str(birth_day)
    class Meta:
        unique_together = ('name', 'surname','birth_day')


'''
Book:
    Title: Title of the book
    Authors: Set of Authors
    lc_classification: Library of congress classification information for this book
'''
class Book(models.Model):
    title = models.CharField(max_length=75)
    authors = models.ManyToManyField(Author, related_name='books')
    lc_classification = models.CharField(max_length=25)
    def __uniceode__(self):
        return title,' ',lc_classification
    class Meta:
        unique_together = ('title', 'lc_classification')
