from datetime import datetime
from libraryapp.library.models import Book, Author


def book_create(info):
    #parsing
    data = info.split(',')
    title = data[0]
    lc_classification = data[1]
    return Book(title=title,lc_classification=lc_classification)

def add_authors(book, infos):
    data = infos.split(',')[2:]
    author_num = 0
    if len(data) % 3 == 0:
        while (author_num*3) < len(data):
            author = create_author(data[author_num*3:])
            book.authors.add(author)
            book.save()
            author_num += 1

def create_author(infos):
    name = infos[0]
    surname = infos[1]
    birth_day = datetime.strptime(infos[2], '%Y-%m-%d')
    author = Author.objects.get_or_create(name=name, surname=surname,
                                          birth_day=birth_day)
    return author[0]
