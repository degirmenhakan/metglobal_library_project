
from rest_framework import serializers
from libraryapp.library.models import Book, Author


class BookSerializer (serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = ('title', 'lc_classification','authors')

class AuthorSerializer (serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('name', 'surname', 'birth_day')
