from django.conf.urls import url
#from libraryapp.library.views import (get_books, get_book, get_library,\
#                           get_authors, get_author)
from libraryapp.library.views import (BookView, AuthorView, \
                                        BookDetailView, AuthorDetailView,
                                        LibraryView)

urlpatterns = [
    url(r'book/$', BookView.as_view(), name='book'),
    url(r'book/(?P<pk>\d+)/$', BookDetailView.as_view(), name='book_detail'),
    url(r'author/(?P<pk>\d+)/$', AuthorDetailView.as_view(), name='author_detail'),
    url(r'author/$', AuthorView.as_view(), name='author'),
    url(r'library/$', LibraryView.as_view(), name='library'),
]
