# -*- coding: utf-8 -*-
from swagger_client.apis.public_operations_api import PublicOperationsApi
from swagger_client.apis.author_add_api import AuthorAddApi
from swagger_client.apis.book_add_api import BookAddApi
from swagger_client.apis.update_add_api import UpdateAddApi
from swagger_client.apis.author_api import AuthorApi
from swagger_client.configuration import Configuration

import requests

conf = Configuration()
url = conf.host

def library_post():
    path = raw_input('Enter the csv file path:')
    conf = Configuration()
    url = conf.host
    response = requests.post(url + '/library/', \
                             files={"csv_data": open(path, 'r')})
    return response

def library_patch():
    path = raw_input('Enter the csv file path:')
    response = requests.patch(url + '/library/', \
                             files={"csv_data": open(path, 'r')})
    return response

def book_get():
    try:
        api = PublicOperationsApi()
        response = api.search_book()
        return response
    except:
        return e.message

def book_post():
    api = BookAddApi()
    data = raw_input('Enter the book infos:')
    res = api.addnewabook(data=data)
    context = {'data':data}
    response = requests.post(url + '/book/', context)
    return response

def author_get():
    api = PublicOperationsApi()
    response = api.search_author()
    return response

def author_post():
    data = raw_input('Enter the author infos:')
    request_query = url+'/author/'
    response = requests.post(request_query)
    return response

def book_detail_get():
    api = PublicOperationsApi()
    id_ = raw_input('Enter the book id: ')
    response = api.search_the_book(int(id_))
    return response

def book_detail_patch():
    id_ = raw_input('Enter the book id: ')
    data = raw_input('Enter the book infos: ')
    context = {'data':data}
    response = requests.patch(url + '/book/'+id_+'/', context)
    return response

def book_detail_put():
    id_ = raw_input('Enter the book id: ')
    data = raw_input('Enter the book infos: ')
    context = {'data':data}
    response = requests.patch(url + '/book/'+id_+'/', context)
    return response

def author_detail_get():
    id_ = raw_input('Enter the author id: ')
    request_query = url+'/author/'+id_+'/'
    response = requests.get(request_query)
    return response

def author_detail_patch():
    id_ = raw_input('Enter the author id: ')
    data = raw_input('Enter the author infos: ')
    context = {'data':data}
    response = requests.patch(url + '/author/'+id_+'/', context)
    return response

def author_detail_put():
    id_ = raw_input('Enter the author id: ')
    data = raw_input('Enter the author infos: ')
    context = {'data':data}
    response = requests.put(url + '/author/'+id_+'/', context)
    return response

def initial_info():
    text = """
                    LIBRARY APIS
            ___________________________
            A library is a a set books which are produced by a set of authors.\
Data definitions you will need is:

                Models
            ---------------
            *Book:
                Title: Title of the book
                Authors: Set of Authors
                lc_classification: Library of congress classification
                information for this book

            *Author:
                Name
                Surname
                Date of birth

                End Points
            -----------------
                /library/
                    POST: CSV file, in the body, removes everything from the\
database and creates a new one with all books ands authors from the database.
                    PATCH: CSV file, in the body, adding just these books, \
with the author.

                /book/
                    GET: List of books. Can be filtered with a regex “filter” \
parameter from the query string
                    POST: New book

                /author/
                    GET: List of authors. Can be filtered with a regex “filter”\
parameter from the query string
                    POST: New author

                /book/{id}/
                    GET: Book details
                    PATCH: Update this book with a set of details
                    PUT: Replace all fields of this book

                /author/{id}/
                    GET: Author details
                    PATCH: Update this author with a set of details
                    PUT: Replace all fields of this author


    """
    print text


def menu():
    text = """
    [1]     /library/       POST
    [2]     /library/       PATCH
    [3]     /book/          GET
    [4]     /book/          POST
    [5]     /author/        GET
    [6]     /author/        POST
    [7]     /book/{id}      GET
    [8]     /book/{id}      PATCH
    [9]     /book/{id}      PUT
    [10]    /author/{id}    GET
    [11]    /author/{id}    PATCH
    [12]    /author/{id}    PUT
    [13]    quit
    """
    print text

def run():
    while True:
        menu()
        try:
            choose = int(raw_input('    '))
            if choose == 1:
                response = library_post()
                print response
            elif choose == 2 :
                response = library_patch()
                print response
            elif choose == 3:
                response = book_get()
                print response
            elif choose == 4:
                response = book_post()
                print response
            elif choose == 5:
                response = author_get()
                print response
            elif choose == 6:
                response = author_post()
                print response
            elif choose == 7:
                response = book_detail_get()
                print response
            elif choose == 8:
                response = book_detail_patch()
                print response
            elif choose == 9:
                response = book_detail_patch()
                print response
            elif choose == 10:
                response = author_get()
                print response
            elif choose == 11:
                response = author_detail_patch()
                print response
            elif choose == 12:
                response = author_detail_put()
                print response
            elif choose == 13:
                print "    QUIT"
                break
            else:
                print 'ERROR: Invalid chhosing'
        except Exception as e:
            print "ERROR: " + e.message

def main():
    initial_info()
    run()


main()
