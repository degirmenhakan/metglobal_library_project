# swagger_client.LibraryAddApi

All URIs are relative to *http://127.0.0.1:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addnewlibrary**](LibraryAddApi.md#addnewlibrary) | **POST** /library/ | add new library


# **addnewlibrary**
> addnewlibrary(file)

add new library

Adds book and authors

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.LibraryAddApi()
file = '/path/to/file.txt' # file | The uploaded file data

try: 
    # add new library
    api_instance.addnewlibrary(file)
except ApiException as e:
    print("Exception when calling LibraryAddApi->addnewlibrary: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **file**| The uploaded file data | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

