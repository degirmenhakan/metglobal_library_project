# swagger_client.PublicOperationsApi

All URIs are relative to *http://127.0.0.1:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**search_author**](PublicOperationsApi.md#search_author) | **GET** /author/ | searches author
[**search_book**](PublicOperationsApi.md#search_book) | **GET** /book/ | searches book
[**search_the_book**](PublicOperationsApi.md#search_the_book) | **GET** /book/{id}/ | searches book
[**search_theauthor**](PublicOperationsApi.md#search_theauthor) | **GET** /author/{id} | searches author


# **search_author**
> list[AuthorItem] search_author()

searches author

get the author list 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicOperationsApi()

try: 
    # searches author
    api_response = api_instance.search_author()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicOperationsApi->search_author: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[AuthorItem]**](AuthorItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_book**
> list[BookItem] search_book()

searches book

get the author list 

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicOperationsApi()

try: 
    # searches book
    api_response = api_instance.search_book()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicOperationsApi->search_book: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[BookItem]**](BookItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_the_book**
> list[BookItem] search_the_book(id)

searches book

get the book

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicOperationsApi()
id = 56 # int | The book info

try: 
    # searches book
    api_response = api_instance.search_the_book(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicOperationsApi->search_the_book: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The book info | 

### Return type

[**list[BookItem]**](BookItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **search_theauthor**
> list[AuthorItem] search_theauthor(id)

searches author

get the author

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.PublicOperationsApi()
id = 56 # int | The book info

try: 
    # searches author
    api_response = api_instance.search_theauthor(id)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling PublicOperationsApi->search_theauthor: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The book info | 

### Return type

[**list[AuthorItem]**](AuthorItem.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

