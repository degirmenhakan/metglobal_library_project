# swagger_client.AuthorApi

All URIs are relative to *http://127.0.0.1:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**replace_author_with_csv**](AuthorApi.md#replace_author_with_csv) | **PUT** /author/{id} | Replace author in the store with form data
[**update_author_with_csv**](AuthorApi.md#update_author_with_csv) | **PATCH** /author/{id} | Updates author in the store with form data


# **replace_author_with_csv**
> replace_author_with_csv(id, data=data)

Updates a author in the store with form data



### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AuthorApi()
id = 56 # int | The author info
data = swagger_client.AuthorItem() # AuthorItem | Author item to update (optional)

try: 
    # Updates a author in the store with form data
    api_instance.replace_author_with_csv(id, data=data)
except ApiException as e:
    print("Exception when calling AuthorApi->replace_author_with_csv: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The author info | 
 **data** | [**AuthorItem**](AuthorItem.md)| Author item to update | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/text/cvs
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **update_author_with_csv**
> update_author_with_csv(id, data=data)

Updates a author in the store with form data



### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.AuthorApi()
id = 56 # int | The author info
data = swagger_client.AuthorItem() # AuthorItem | Author item to update (optional)

try: 
    # Updates a author in the store with form data
    api_instance.update_author_with_csv(id, data=data)
except ApiException as e:
    print("Exception when calling AuthorApi->update_author_with_csv: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| The author info | 
 **data** | [**AuthorItem**](AuthorItem.md)| Author item to update | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/text/cvs
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

