# swagger_client.UpdateAddApi

All URIs are relative to *http://127.0.0.1:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**updatelibrary**](UpdateAddApi.md#updatelibrary) | **PATCH** /library/ | update library


# **updatelibrary**
> updatelibrary(file)

update library

Update library

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.UpdateAddApi()
file = '/path/to/file.txt' # file | The uploaded file data

try: 
    # update library
    api_instance.updatelibrary(file)
except ApiException as e:
    print("Exception when calling UpdateAddApi->updatelibrary: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file** | **file**| The uploaded file data | 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

