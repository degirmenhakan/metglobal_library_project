# swagger_client.BookAddApi

All URIs are relative to *http://127.0.0.1:8000/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addnewabook**](BookAddApi.md#addnewabook) | **POST** /book/ | add new book


# **addnewabook**
> addnewabook(data=data)

add new book

Adds an item to the author

### Example 
```python
from __future__ import print_statement
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.BookAddApi()
data = swagger_client.BookItem() # BookItem | Book item to add (optional)

try: 
    # add new book
    api_instance.addnewabook(data=data)
except ApiException as e:
    print("Exception when calling BookAddApi->addnewabook: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **data** | [**BookItem**](BookItem.md)| Book item to add | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/text/cvs
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

