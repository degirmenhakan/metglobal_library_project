# coding: utf-8

"""
    Simple Library API

    This is a simple API

    OpenAPI spec version: 1.0.0
    Contact: hakan.degirmen@metglobal.com
    Generated by: https://github.com/swagger-api/swagger-codegen.git

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""

from pprint import pformat
from six import iteritems
import re


class AuthorItem(object):
    """
    NOTE: This class is auto generated by the swagger code generator program.
    Do not edit the class manually.
    """
    def __init__(self, id=None, name=None, surname=None, birth_day=None):
        """
        AuthorItem - a model defined in Swagger

        :param dict swaggerTypes: The key is attribute name
                                  and the value is attribute type.
        :param dict attributeMap: The key is attribute name
                                  and the value is json key in definition.
        """
        self.swagger_types = {
            'id': 'int',
            'name': 'str',
            'surname': 'str',
            'birth_day': 'date'
        }

        self.attribute_map = {
            'id': 'id',
            'name': 'name',
            'surname': 'surname',
            'birth_day': 'birth_day'
        }

        self._id = id
        self._name = name
        self._surname = surname
        self._birth_day = birth_day


    @property
    def id(self):
        """
        Gets the id of this AuthorItem.


        :return: The id of this AuthorItem.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id):
        """
        Sets the id of this AuthorItem.


        :param id: The id of this AuthorItem.
        :type: int
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")

        self._id = id

    @property
    def name(self):
        """
        Gets the name of this AuthorItem.


        :return: The name of this AuthorItem.
        :rtype: str
        """
        return self._name

    @name.setter
    def name(self, name):
        """
        Sets the name of this AuthorItem.


        :param name: The name of this AuthorItem.
        :type: str
        """
        if name is None:
            raise ValueError("Invalid value for `name`, must not be `None`")

        self._name = name

    @property
    def surname(self):
        """
        Gets the surname of this AuthorItem.


        :return: The surname of this AuthorItem.
        :rtype: str
        """
        return self._surname

    @surname.setter
    def surname(self, surname):
        """
        Sets the surname of this AuthorItem.


        :param surname: The surname of this AuthorItem.
        :type: str
        """
        if surname is None:
            raise ValueError("Invalid value for `surname`, must not be `None`")

        self._surname = surname

    @property
    def birth_day(self):
        """
        Gets the birth_day of this AuthorItem.


        :return: The birth_day of this AuthorItem.
        :rtype: date
        """
        return self._birth_day

    @birth_day.setter
    def birth_day(self, birth_day):
        """
        Sets the birth_day of this AuthorItem.


        :param birth_day: The birth_day of this AuthorItem.
        :type: date
        """
        if birth_day is None:
            raise ValueError("Invalid value for `birth_day`, must not be `None`")

        self._birth_day = birth_day

    def to_dict(self):
        """
        Returns the model properties as a dict
        """
        result = {}

        for attr, _ in iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value

        return result

    def to_str(self):
        """
        Returns the string representation of the model
        """
        return pformat(self.to_dict())

    def __repr__(self):
        """
        For `print` and `pprint`
        """
        return self.to_str()

    def __eq__(self, other):
        """
        Returns true if both objects are equal
        """
        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """
        Returns true if both objects are not equal
        """
        return not self == other
